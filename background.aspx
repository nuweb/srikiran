<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
aboutus.Text = thispage;
background.Text = "sel";
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - About Us</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
	$('#navlist').show();	
});
</script>  
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro2" style="position:relative">
    		<p style="margin:70px 0 0 20px; font-size:13px; position:absolute"><span style="color:#FFCC66">At Srikiran we are proud to announce that- <br />"No person is denied treatment for not being able to pay".</span></p>
    </div>    
    <p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="aboutUs.aspx">About us</a> <span class="arrow1">&nbsp;</span> <a href="background.aspx">Background of Srikiran Institute of Ophthalmology</a></p>
	<div id="content">

		<div id="main">
            <h2>Background of Srikiran Institute of Ophthalmology</h2>
			<p>Sankurathri Foundation was established in 1989 to provide necessary tools for the underprivileged persons in order to improve the quality of their life. The Foundation was registered as a Trust under the rules and obtained the Income Tax Exemption under US 80G of Income Tax Act, 1961. Three volunteer Trustees manage the Foundation and all the activities are planned and supervised by the Executive Trustee, who stays at Kakinada.</p>
			<p>Sankurathri Foundation was established by Dr.Chandrasekhar Sankurathri as a memorial to his wife, Manjari Sankurathri, son, Srikiran and daughter, Sarada who were killed in a terrorist bombing of Air India Flight 182 (Kanishka) on 23rd June 1985. <b>The Foundation's main mission is to empower the poor through better education, eye care and timely help to the needy.</b></p>
			<p>M/S. Sankurathri Foundation established Srikiran Institute with the objective of providing quality eye care with compassion, which is affordable and accessible to all. The Institute is located in a serene location of about 5 acres in a rural setting, 10 km from Kakinada.</p>
			<p>Srikiran, as it is popularly known in the area is provided with State-of-the art equipment and facilities to provide the quality eye care. Since last 16 years Srikiran has provided outpatient services to over 1,370,670 patients and performed over 1,42,358 surgeries. </p>
			<p>Srikiran takes eye care to the doorsteps of the needy through community out-reach programs in remote and rural areas of East Godavari, West Godavari, Visakhapatnam, Vizianagaram, Srikakulum and Khammam districts. Thus far more than 1,290 screening camps for adults and 792 screening camps for school children were conducted. Thus Srikiran is serving a total population of about 20 million.</p>
			<p>Srikiran provides training to Ophthalmologists and Ophthalmic Assistants. The Government of India recognizes Srikiran as one of the eleven training centers for Ophthalmologists. Srikiran performs about 65% of the total cataract surgeries conducted in East Godavari District. For their efforts Srikiran received the best Non Governmental Organization award for performing maximum number of cataract surgeries, successively for two years, in 2002 and 2003 by the Government of Andhra Pradesh. During 2002 and 2003 Srikiran has performed about 17,500 surgeries per year, which is a very commendable contribution towards the elimination of blindness.</p>
			<p>Srikiran has a built up areas of 50,000 square feet distributed in five buildings in the same compound. The facilities include separate outpatient areas for paid and free patients, 10 outpatient clinics, four operating theaters, one generator room, one auditorium, one library, one canteen, one optical shop, one optical manufacturing facility, one pharmacy and two hostels, for boys and girls. At Srikiran 150 beds for inpatients and 24 hours emergency services are available. Now Srikiran has the capacity to provide eye care services to about 20 million populations from six districts of Andhra Pradesh.</p>
			<p>The Institute has added the new facility and developed into a tertiary eye care center in the region with all specialties like cataract, cornea, Glaucoma, Retina, pediatric, microbiology laboratory, low vision rehabilitation, including an eye bank. There is an auditorium to conduct workshops and seminars for the public and professionals. Very recently Srikiran has also launched teleophthalmology program and corneal transplantations.</p>
			<p>The Institute at present has eight ophthalmologists and one SICS trainee. In addition there are 97 full time employees (6 computer, 15 administration, 18 vision technicians, 6 outpatient, 16 operation theater, 10 ward, 8 transportation and 18 support services). There are 36 trainees in various departments getting training in various sections of the eye hospital. </p>
			<p>Srikiran has developed the software to maintain the patient data and to generate the necessary reports. The data is maintained in a dedicated server and is accessible through network in the Institute.</p>
			
            <h3>The Institute has the following equipment:</h3>
			<table id="mytable" cellspacing="0">
        <caption><strong></strong></caption>
          <tr>
            <th class="firstcell">Equipment</th>        
            <th width="100">Quantity</th>
          </tr>
          <tr>
            <th class="specalt"><strong>Autorefractometers</strong></th>
            <td class="alt">8</td>
          </tr>
          <tr>
            <th class="spec"><strong>Auto chart Projectors</strong></th>
            <td>8</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Remote Vision Drums</strong></th>
            <td class="alt">8</td>
          </tr>
          <tr>
            <th class="spec"><strong>Manual Vision drums</strong></th>
            <td>10</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Trail sets</strong></th>
            <td class="alt">15</td>
          </tr>
          <tr>
            <th class="spec"><strong>Slit lamps</strong></th>
            <td>15</td>
          </tr>
          <tr>
            <th class="specalt">Ophthalmoscopes</th>
            <td class="alt">9</td>
          </tr>
          <tr>
            <th class="spec">Indirect Ophthalmoscopes </th>
            <td>8</td>
          </tr>
          <tr>
            <th class="specalt">Tonopens</th>
            <td class="alt">2</td>
          </tr>
          <tr>
            <th class="spec">Retinoscopes</th>
            <td>12</td>
          </tr>
          <tr>
            <th class="specalt">Perkins Tonometers </th>
            <td class="alt">12</td>
          </tr>
          <tr>
            <th class="spec">Keratometers</th>
            <td class="alt">4</td>
          </tr>
          <tr>
            <th class="specalt">Pachymeter</th>
            <td class="alt">2</td>
          </tr>
          <tr>
            <th class="spec">A/B-Scan</th>
            <td class="alt">3</td>
          </tr>
          <tr>
            <th class="specalt">A-Scans</th>
            <td class="alt">5</td>
          </tr>
          <tr>
            <th class="spec">Humphrey’s Visual field analyzer</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Haag-Streit Goldman perimeter</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="spec">Portable slit lamp</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Argon laser</th>
            <td class="alt">2</td>
          </tr>
          <tr>
            <th class="spec">Corneal topography unit</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Yag lasers</th>
            <td class="alt">2</td>
          </tr>
          <tr>
            <th class="spec">Endolaser</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Operating microscopes</th>
            <td class="alt">7</td>
          </tr>
          <tr>
            <th class="spec">Weck microscope</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Occutome</th>
            <td class="alt">2</td>
          </tr>
          <tr>
            <th class="spec">Phacoemlsification units</th>
            <td class="alt">3</td>
          </tr>
          <tr>
            <th class="specalt">Vitrectomy machine</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="spec">Cataract Sets</th>
            <td class="alt">75</td>
          </tr>
          <tr>
            <th class="specalt">Ultrasonic Cleaners</th>
            <td class="alt">3</td>
          </tr>
          <tr>
            <th class="spec">Anesthesia apparatus</th>
            <td class="alt">1</td>
          </tr>
          <tr>
            <th class="specalt">Pulsoximeter</th>
            <td class="alt">3</td>
          </tr>
          <tr>
            <th class="spec">Instaclaves</th>
            <td class="alt">3</td>
          </tr>
          <tr>
            <th class="specalt">Autoclaves</th>
            <td class="alt">3</td>
          </tr>
        </table>            

		<p>Srikiran is very much interested in developing this Institute into a teaching center for eye care professionals and support personnel. Thus far Srikiran has trained 45 ophthalmologists, 210 paramedical personnel and 98 other personnel. Many volunteers from Canada and USA visit Srikiran regularly and provide training for medical and paramedical personnel in latest diagnostic and surgical skills. During those visits Srikiran organizes workshops and seminars on various topics related to the treatments and surgical techniques. Thus far 47 such workshops were conducted and attended by many ophthalmologists, pediatricians and general physicians. </p>
		<p>Srikiran Institute of Ophthalmology is heading in the direction of becoming a center of excellence, which can serve a population of 20 million from the six coastal districts of Andhra Pradesh. Srikiran is in tune with Vision 2020 programs and offering the technical help in training ophthalmologists and ophthalmic assistants, in reducing the backlog of cataract blindness, to participate in prevention of childhood blindness and other related activities. </p>
		<p>Srikiran-ORBIS Pediatric Ophthalmology Program was initiated from August 2004, where dedicated facilities are available for children’s eye care. Srikiran also has four vision centers to provide eye care to the rural populations and an active participant in Vision 2020 India forum. </p>
		<p>Srikiran has developed the software to maintain the patient data and to generate the necessary reports. The data is maintained in a dedicated server and is accessible through network in the Institute.</p>
     

		</div>
		
		<!-- #INCLUDE file="right_nav.aspx" -->
	
	</div>	
</div>

<!-- #INCLUDE file="footer.aspx" -->		 	
</body>
</html>