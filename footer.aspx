﻿<div id="footer">
  <ul>
    <li><a href="index.aspx"><span>Home</span></a></li>
    <li><a href="aboutUs.aspx"><span>About Us</span></a></li>
    <li><a href="services.aspx"><span>Services</span></a></li>
    <li><a href="programs.aspx"><span>Programs</span></a></li>
    <li><a href="staff.aspx"><span>Staff</span></a></li>
    <li><a href="news.aspx"><span>News</span></a></li>
    <li><a href="volunteer.aspx"><span>Volunteer</span></a></li>
    <li><a href="donate.aspx"><span>Donate</span></a></li>
    <li><a href="contact.aspx"><span>Contact</span></a></li>
  </ul>
  <p>Copyright &copy; 2009 Srikiran.org - All Rights Reserved</p>
</div>
