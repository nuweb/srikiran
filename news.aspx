<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
news.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - News</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro8" style="position:relative">
    		<div style="margin:20px 0 0 440px; font-size:13px; position:absolute; width:450px; font-family:'Trebuchet MS'; line-height:21px">
            <p style="font-size:16px"><b>REAL HEROES OF INDIA</b></p>
            <p>The selfless acts of these unsung heroes from across the nation in various fields as women's welfare, social welfare, health &amp; disability, Education and Sports will inspire the nation and prove that with determination and self belief even an ordinary person can have a profound impact on our society.</p><p>On March 10th all the Real heroes were felicitated at a glittering ceremony at Trident Hotel in Mumbai. The event was sponsored by Reliance Industries and attended by eminent guests, media personalities and corporate chieftains.</p>
            </div>
  </div>    

  <p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="news.aspx">News</a></p>        
	<div id="content">

		<div id="main">
			<h3>REAL HEROES OF INDIA</h3>        
            <p>This event celebrated the undying spirit of ordinary people who have done extraordinary service and have expanded the realm of humanity. This initiative recognized the real life heroes who never gave up against adversities and served the cause close to their hearts. The selfless acts of these unsung heroes from across the nation in various fields as women's welfare, social welfare, health &amp; disability, Education and Sports will inspire the nation and prove that with determination and self belief even an ordinary person can have a profound impact on our society.</p>
            <div style="text-align:center"><object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/e6B3YpwmIsI&hl=en_US&fs=1&color1=0x5d1719&color2=0xcd311b"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/e6B3YpwmIsI&hl=en_US&fs=1&color1=0x5d1719&color2=0xcd311b" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object></div>
            <p>On March 10th all the Real heroes were felicitated at a glittering ceremony at Trident Hotel in Mumbai. The event was sponsored by Reliance Industries and attended by eminent guests, media personalities and corporate chieftains. These honors were bestowed after an extensive search across the country and the awards were selected from very diverse fields.</p>
          <div style="text-align: center; font-family: verdana; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none;"><img src="photos/real_heroes.jpg" border="0" width="430" height="284" /><div>
            <p><em> Industrialist Mukesh Ambani, Sachin Tendulkar and Aamir Khan </em></p>
            <p><em>along with Dr.   Chandrasekhar Sankurathri</em></p>
          </div>
          </div>
            <p>In this event, Dr.Chandrasekhar Sankurathri, Founder and Executive Trustee of Sankurathri Foundation in India and Founder and President of Manjari Sankurathri Memorial Foundation in Canada was also felicitated for his contribution in Social Welfare category. His services towards the elimination of illiteracy and blindness to poor and needy have contributed towards this award. He is the only individual to receive this prestigious award from Andhra Pradesh.</p>
            <p>Under the banner of Sankurathri Foundation, he has steered two major programs, education to rural children and quality eye care to poor and needy. The education is completely free for all children from Sarada Vidyalayam while 90% of the surgeries are completely free of cost to the needy and poor through Srikiran Institute of Ophthalmology.</p>
		  <p>While commenting on this award, Dr.Chandrasekhar said that this special award is a good recognition to the work undertaken since last 20 years and this award he is dedicating to all the staff members because without their commitment and dedication nothing would have been possible. He also donated all the award money to the Sankurathri Foundation in order to strengthen the efforts towards the eradication of illiteracy and blindness.</p>
		</div>		
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->		 	
</body>
</html>