﻿<style>
    #topnav a:link		{color:#fff; text-decoration:none; }
    #topnav a:visited	{color:#F8F8F8; text-decoration:none; }
    #topnav a:hover		{color:#FFCC66; text-decoration:underline; }
    #topnav a.selected	{color:#FFCC66; text-decoration:underline; }
    #topnav a:active	{color:#FFCC33; text-decoration:none; }
</style>
<div id="header">
  <div id="logo" class="pngns" onclick="javascript:document.location.href='index.aspx'"></div>
<div id="topnav" style="position:absolute; top:5px; right:0; font:normal 11px Verdana; color:#fff">
    <a class="<asp:Literal id='volunteer' runat='server' />" href="volunteer.aspx"><span>Volunteer</span></a> | <a class="<asp:Literal id='donate' runat='server' />" href="donate.aspx"><span>Donate</span></a> | <a class="<asp:Literal id='contact' runat='server' />" href="contact.aspx"><span>Contact</span></a>
</div>
<ul id="nav">
    <li class="<asp:Literal id='home' runat='server' />"><a href="index.aspx"><span>Home</span></a></li>
    <li class="<asp:Literal id='aboutus' runat='server' />"><a href="aboutUs.aspx"><span>About Us</span></a></li>
    <li class="<asp:Literal id='services' runat='server' />"><a href="services.aspx"><span>Services</span></a></li>
    <li class="<asp:Literal id='programs' runat='server' />"><a href="programs.aspx"><span>Programs</span></a></li>
    <li class="<asp:Literal id='staff' runat='server' />"><a href="staff.aspx"><span>Staff</span></a></li>
    <li class="<asp:Literal id='news' runat='server' />"><a href="news.aspx"><span>News</span></a></li>    
</ul>
</div>


