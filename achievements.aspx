<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
aboutus.Text = thispage;
achievements.Text = "sel";
}
</script><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - About Us</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
	$('#navlist').show();	
});
</script>  
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro2" style="position:relative">
    		<p style="margin:70px 0 0 20px; font-size:13px; position:absolute"><span style="color:#FFCC66">At Srikiran we are proud to announce that- <br />"No person is denied treatment for not being able to pay".</span></p>
    </div>    
	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="aboutUs.aspx">About us</a> <span class="arrow1">&nbsp;</span> <a href="background.aspx">Srikiran's Achievements up to December 2008</a></p>    
	<div id="content">

		<div id="main">
            <h2>Srikiran's Achievements up to December 2008</h2>
			<ul>
                <li>Established a High School for the rural children to provide quality added education.</li>
                <li>More than 1,375 children from the rural areas are educated.</li>
                <li>Provided scholarships for over 150 children from the poor families to continue their education at High schools and Colleges.</li>
                <li>Established a state-of-the-art eye care facility in the area.</li>
                <li>Provided eye care services to 1,370,607 patients from a population of 10 million. Among them 76% are poor and they received free consultations.</li>
                <li>Performed 1,52,909 surgeries which include 91% free surgeries for poor and needy. This is a very unique feature of this organization.</li>
                <li>Organized 1,450 free eye screening camps in rural areas.</li>
                <li>Out Patients Treated in camps: 14,97,725</li>
                <li>Number of outpatients at the Hospital: 513,258</li>
                <li>Conducted 891 children's eye screening camps.
                <li>Screened 2,39,175 children.</li>
                <li>Conducted 63 workshops for doctors.</li>
                <li>Provided training to 72 Ophthalmologists.</li>
                <li>Trained 298 Paramedical personnel.</li>
                <li>Trained more than 98 support personnel.</li>
                <li>Established 5 vision centers covering a population of 500,000.</li>
                <li>Number of city centers established: 1</li>
                <li>More than 45 international volunteers participated in various activities.</li>
			</ul>
            <p>Srikiran wishes to acknowledge the cooperation and help of many organizations in restoring eyesight to many needy people in the area. They have contributed for the development of the Institute by improving the quality care, training programs, supporting various programs, providing equipment and technical skills. They are as follows:</p>
			<ul>
            <li>Aravind Eye care Systems, Madurai.</li>
            <li>Canadian International Development Agency (CIDA), Canada.</li>
            <li>Christoffel- Blindenmission (CBM), Benshiem, Germany.</li>
            <li>District Blindness Control Society DBCS).</li>
            <li>Eye Foundation of America, Morgantown, West Virginia, USA.</li>
            <li>Eye Care for the Adirondacks, Plattsburgh, New York, USA.</li>            
            <li>Help Age India.</li>
            <li>Help The Aged Canada, Ottawa, Canada.</li>
            <li>Infosys Foundation, India.</li>            
            <li>Manjari Sankurathri memorial Foundation, Ottawa, Canada.</li>
            <li>National Program for Control of Blindness (NPCB).</li>
            <li>ORBIS International, New York, U.S.A.</li>            
			<li>Rotary International.</li>
            <li>University of Ottawa Eye Institute</li>
            </ul>
			
            <p>Srikiran Institute of Ophthalmology can achieve future objectives and participate in Vision 2020 to eradicate avoidable blindness with the cooperation, support, guidance and input from other eye care organizations, Ophthalmologists and other individuals.</p>

			<p>For enquiry or for further information please contact the following:</p>
            <p>Managing Director,<br />
            <b>Sankurathri Foundation</b>,<br />
            Near Atchampeta Junction, Penumarthy Road,<br />
            Kakinada &ndash; 533005,<br />
            Andhra Pradesh, INDIA<br />
            Tel: (0884)-230-6301<br />
            Fax: (0884)-230-6345<br />
            E-mail: <a href="mailto:chandra@srikiran.org">chandra@srikiran.org</a><br />
            </p>

		</div>
		
		<!-- #INCLUDE file="right_nav.aspx" -->	
	
	</div>	
</div>
	<div id="footer">
	
		<ul>
			<li><a href="index.aspx"><span>Home</span></a></li>
			<li><a href="aboutUs.aspx"><span>About Us</span></a></li>
			<li><a href="services.aspx"><span>Services</span></a></li>
			<li><a href="programs.aspx"><span>Programs</span></a></li>
			<li><a href="staff.aspx"><span>Staff</span></a></li>        
            <li><a href="volunteer.aspx"><span>Volunteer</span></a></li>    
       		<li><a href="donate.aspx"><span>Donate</span></a></li>
			<li><a href="contact.aspx"><span>Contact</span></a></li>
		</ul>

	
		<p>Copyright &copy; 2008 Srikiran.org - All Rights Reserved</p>

	
	</div>
</body>
</html>