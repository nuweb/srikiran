<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
staff.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Staff</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
<style type="text/css" media="screen">
	ul, ol {
		padding-left: 20px;
	}
</style>
</head>
<body>

<div id="container">
<!-- #INCLUDE file="header.aspx" -->		 	
<div id="intro2" style="position:relative">
<p style="margin:70px 0 0 20px; font-size:13px; position:absolute"><span style="color:#FFCC66">At Srikiran we are proud to announce that- <br />"No person is denied treatment for not being able to pay".</span></p>
</div>  
<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="staff.aspx">Staff</a></p>
<div id="content">
<div id="main">
<h2>Meet the Staff</h2>
<ul class="items">

<li class="first">
<h4><img src="photos/dr.avinash.png" alt="Dr.Avinash">Dr.Avinash Mahindrakar - MBBS, M.D, DNB,  FRCS, Fellowship In Ocular Motility and Gen. Oph.</h4>					
<h3>Medical Director, Oculoplasty, Squint, Pediatric and Phaco Surgeon.</h3>
<p>
<ul class="bullets">
<li>Completed his MBBS from Osmania University in 1996.</li>
<li>MD - Ophthalmology from AIIMs in 2000</li>
<li>DNB Ophthalmology from National board of Examinations in 2000</li>
<li>FRCS Royal College of Physicians and Surgeons of Glasgow in 2002</li>
<li>Senior Residency (Oculoplasty, Pediatric Ophthalmology &amp; Tumors and Fellowship in Ocular Motility and General Ophthalmology from Royal Victorian Eye &amp; Ear Hospital - Melbourne in 2009.</li>
<li>He has experience of 11 years and worked at various Prestigious institutes like AIIMS, Royal Victorian Eye and Ear Hospital - Melbourne as a senior resident and as consultant at Lions Charitable Eye Hospital for 4 years and Seven Hills Hospital - Visakhapatnam for a period of Five years Six months.</li>
<li>With his teaching experience he is guiding and mentoring the aspiring DNB students.  And upon joining the institute he has performed many Squint, Oculoplasty surgeries, Ptosis  and trauma surgeries like lid tear and others.</li>
</ul>
</p>
<p class="no-pad">
<strong>PUBLICATIONS</strong>
<ol>
<li>Effectivenes of Mitomycin C in reducing reformation of adhesions following surgery for restrictive strabismus. Journal of Pediatric Ophthalmology and Strabsimus May/June 2001 Vol. 38, No.3, page no.s 131-135. Avinash Mahindrakar, Radhika Tandon, V. Menon, Pradeep Sharma, Sudershan Khokhar.</li>
<li>Disseminated hydatid disease involving orbit, spleen, lung and liver. Ophthalmologica November 2002; 216:300-304.  Betheria SM, Neelam Pushkar, Vidushi Sharma, Avinash Mahindrakar, Seema Kashyap</li>
<li>Mitomycin C in restrictive strabismus. Reply to letter. Journal of Pediatric Ophthalmology and Strabsimus Sept-Oct 2002; 39(5):260. Avinash Mahindrakar, Radhika Tandon, Pradeep Sharma, Vimla Menon.</li>
<li>Dentigerous cyst in maxillary sinus - a rare cause of nasolacrimal duct obstruction. Orbit December 2003; 22 (4): 289-292. Bajaj MS, Avinash Mahindrakar, Neelam Pushker, Balasubramanya R.</li>
<li>Advancement of Whitnall's ligament via the conjunctival approach for correction of congenital ptosis. Orbit September 2004; 23 (3):153-9. Bajaj MS, Pushker N, Avinash Mahindrakar, Balasubramanya R.</li>
<li>Pleomorphic adenoma of the lacrimal gland: a clinicopathological analysis.Clinical and Experimental Ophthalmology October 2004; 32 (5):523-5. Sen S, Avinash Mahindrakar, Betharia SM, Bajaj MS, Kashyap S, Ghose S.</li>
<li>Chabra MS. Orbital ectopic glial tissue in relation to medial rectus: a rare entity. Clinical and Experimental Ophthalmology 2005 February; 33 (1): 67-69. Ghose S, Sen S, Balasubramaniam ST, Avinash Mahindrakar, Sharma V</li>
</ol>
</p>


</li>



<li>
<h4><img src="images/dr_tulasi.png" alt="Dr.B.Thulasi">Dr.B.Thulasi - MBBS, D.O</h4>
<h3>Comprehensive Ophthalmology</h3>
<p>
<ul class="bullets">
<li>Completed her graduation from Sri Venkateswara University in 1977,</li>
<li>D.O from Madras University in 1980.</li>
<li>Cornea fellowship from LVPEI, Preceproship from USA.</li>
<li> She is a multi skilled doctor with exposure in different specialties of Ophthalmology and is associated with the institute since past 10 years as senior consultant.</li>
<li>She has a total work experience of 31 works for 21 years she was associated with CMC-Pithapuram as consultant and later on promoted as Medical Superintendent and since 10 years she is associated with our institute.</li>

</p>
</ul>
</li>
<li>

<h4><img src="photos/dr_chandramohan.jpg" alt="Dr.Kolluru Chandra Mohan">Dr.K.Chandra Mohan, MBBS, M.S., DNB, Fellowship in V.R.S Hospital, FRCS</h4>
<h3>Retina Specialist</h3>
<p>
				
<ul class="bullets">
<li>He completed his MBBS from NTR University of Health Sciences in 1996.</li>
<li>Post Graduation (MS- Oph.) from Dr.MGR University of Health Sciences in 2001.</li>
<li>DNB (Oph.) from National Board of Examination in 2002.</li>
<li>Fellowship in V.R.S from Aravind Eye Hospital in 2001.</li>
<li>FRCS from Royal College of Physicians and Surgeons of Glasgow.</li>
<li>Surgical training course on the use of TOPCON OFFISS operating microscope conducted by Topcon Company at Fujita health University, Japan in 2004.</li>
<li>Asst.Prof of Ophthalmology and Vitreo - Retinal Consultant, 2005 - 2009.</li>
<li>Chief Medical Officer and Chief V.R Consultant, Sankara Eye Hospital, 2009-2011.</li> 
</ul>				

<h3>RESEARCH PROJECTS</h3>
<ul class="bullets">					
<li>Comparison of argon and diode laser in scatter photocoagulation treatment of fibrovascular PDR.</li>
<li>Transpupillary thermotherapy (TTT) in treatment of choroidal neovascular  membrane (CNVM) secondary to non-age related macular 
degeneration</li>
<li>Transpupillary thermotherapy (TTT) in patients with choroidal neovascular membranes  secondary to age related macular degeneration</li>
<li>Hyperacuity perimetry in ARMD</li>
<li>Intravitreal Triamcinolone in diabetic macular edema</li>
<li>Intravitreal Triamcinolone in ARMD</li>
<li>OCT in atypical leakage in CSCR</li>
<li>Serum lipid profiles in Diabetic retinopathy patients with CSME and without CSME Prevalence of Diabetic retinopathy in a tertiary eye care 

center</li>
<li>A study comparing the efficacy of Indigenous green laser with conventional green laser</li>
<li>A study comparing the Clinical grading of Diabetic retinopathy with that of a grading software</li>
</ul>

<h3>PUBLICATIONS</h3>
<ul class="bullets">
<li> Chandra Mohan, K, Subburam, N & Natchiar, G, Horner's Syndrome TNOA 1999: 40(4); 23-26</li>
<li> Chandra Mohan K, Vidhya, Bipasha, Usha Kim, Orbital Secondaries from Thyroid - A Rare Presentation TNOA   2002; 43 (1):21-22</li>
<li> Chandra Mohan K,  Shukla Dhananjay, Namperumalsamy P, Kim R, Management of Age-Related Macular Degeneration" J INDIAN MED ASSOC 2003:101 (8) 471-476</li>
<li> Shukla, Dhananjay , Singh Jatinder, Kolluru Chandra Mohan, Kim Ramasamy, Namperumalsamy Perumalsamy, Transpupillary Thermotherapy for Subfoveal Neovascularization Secondary to Group 2A Idiopathic Juxtafoveolar Telangiectasis   AM. J. Oph 2004: 138 (1) 47-149</li>
<li>Shukla Dhananjay, Mohan K Chandra, Rao Nageswara, Kim R, Namperumalsamy P, Cunningham Jr Emmett T , Posterior Scleritis causing Combined Central Retinal Artery and Vein Occlusion RETINA 2004: 24 (3) 
467-469</li>
<li>Shukla, Dhananjay,  Kolluru Chandra Mohan, Singh Jatinder, John, Rajesh K, Soman Manoj, Gandhi Banushree, Kim R, Namperumalsamy Perumalsamy Macular Ischaemia as a Marker for Nephropathy in Diabetic Retinopathy" I J O 2004 : 52(3) 205-210</li>
<li>Shah Parag K, Mamatha S R, Talwadkar Hrishekesh, Chandramohan K, Anil R, Shukla D, Manoj S, Morris Rodney J, Kim R, Namperumalsamy P, Anatomical and Visual Outcome of Paediatric Retinal Detachments following Primary Buckle Surgery TNOA 2004, 92-96</li>
<li>Vedantham Vasumathy, Kolluru Chandramohan and Ramasamy Kim, Persistent Depot of Triamcinolone Acetonide after a Single Intravitreal Injection IJO 2005: 53 (1)65-66</li>
<li>Vedantham V, Kolluru C and Ramasamy K, "Treatment of Polypoidal Choroidal Vasculopathy with Transpupillary Thermotherapy: an interventional case report" EYE 2005:19(8) 915-917</li>
<li>Editor-in Chief of a Text Book on "Retinal Vascular Disorders", published by Academa Publishers, New Delhi in 2005</li>

</li>
</ul>
<h3>MEMBERSHIPS</h3>

<p>Member off All India Ophthalmological Society (AIOS)</p>
<p>Member of Vitreo-Retinal Society of India (VRSI)</p>
<p>Member of Tamil Nadu Ophthalmological Society (TNOA)</p>
<p>Member of National Advisory Board, Novartis India</p>
<p>Member of Andhra Pradesh Ophthalmic Association</p>



<li>
<h4><img src="images/dr_jagan.png" alt="Dr.D.Y.Jaganmohan">Dr.Devanga Yerra Jaganmmohan, M.S.</h4>
<p>Dr.D.Y.Jaganmohan is a certified Ophthalmologist and a Consultant in Srikiran Institute of Ophthalmology. He received his medical degree from NTR University of health Sciences in 1999 and MS in 2003. His experience as an Ophthalmologist includes: Asst.Professor in MRI Medical College for 8 months Ophthalmic Surgeon in Ashoka Eye Hospital, Visakhapatnam for 1 year. Cornea Fellowship at Aravind Eye Hospital for 18 months. General Consultant in Paramahamsa Yogananda Netralaya for 9 months. Presently he is working at Srikiran 
Institute of Ophthalmology as a Consultant and Cornea Specialist from 2008.</p><p>He was awarded Dr.Bommakanti Swamisaran memorial Gold Medal, Dr.Kosaraju Rajeswara Rao Silver Medal and 
Mangaiah Sarma Award.</p>
</li>


<li>

<h4><img src="images/dr_sandeep.jpg" alt="Dr.G.Sandeep Reddy">Dr.G.Sandeep Reddy, M.B.B.S., 

D.O. (Ophthalmology), DNB (ophthalmology)
Vitreoretinal Consultant</h4>

<p>He has completed his M.B.B.S. from Sri Devraj Urs Medical College in 2003 with 1st class. Then he completed his D.O. in ophthalmology from Sarojini Devi Eye Hospital, Hyderabad in 2007 with 1st class.</p>
<p>He has completed D.N.B. in 2010 from Aravind Eye Hospital in 2010. Following that he has completed two year fellowship in Vitreoretinal department at Aravind Eye Hospital.</p>
				</li>
<h3>RESEARCH</h3>
<p>DNB Thesis on "Efficacy of Intravitreal Bevacizumab in the treatment of choroidal neovascularization of various etiologies"</p>
<h3>PAPERS / TALKS PRESENTED </h3>
<ul class="bullets">
							<li>Comparison of PMMA lenses with acrylic hydrophobic iolenses: compatibility in uveitic cataract cases - at Andhra Pradesh state ophthalmic society, Kakinada, 2007</li>
							<li>Comparison of Applanation tonometry and non contact tonometry by central cornea thickness - AIOS 2008, Jaipur</li>
<li>Incidence of blindness among challenged children (deaf and dumb) - AIOS 2009, Kolkata</li>
<li>Instruction course on interpretation of visual fields in PG update, April 2009</li>
						</ul>
<h3>PUBLICATIONS</h3>
<p>Comparison of Applanation tonometry and non contact tonometry by central cornea thickness- Ophthalmology World Report, December 2009</p>
<h3>POSTER PRESENTATION</h3>
<p>Serpigenous choroiditis following chicken pox infection: International uveitis Society Conference, November 2011</p>
<h3>ACADEMIC ACHIEVEMENTS</h3>
<ul class="bullets">
							<li>Qualified Fellow of Royal College surgeons, Glasgow (Part I), February 2007</li>
							<li>Qualified International Council of Ophthalmology (Part II) examination in May 2010</li>
						</ul>




<li>
<h4><img src="photos/dr_chinnayya.jpg" alt="Dr.Chinnayya Rao">Prof. Dr.K.N.Chinnayya Rao- MBBS, D.O.M.S.,  M.S.</h4>

<p>
						<ul class="bullets">
							<li>Completed MBBS from 

Guntur Medical College - Andhra University in 1966</li>
							<li>D.O.M.S from N.I.O.R. 

Sitapur - Agra University in 1968</li>
							<li>M.S in Ophthalmology from 

Guntur Medical College - Andhra University in 1974.</li>
						</ul>
					</p>
					<p><strong>EXPERIENCE</strong>
He has vast experience of 37 years - 26 years in Andhra Pradesh Medical and Health Science 

department (21 years of teaching experience - 5yrs as Asst. Professor and 16 years as Professor) and 

11years as a private practitioner.
					</p>
					<p>
						<strong>PUBLICATIONS</strong>
<ul class="bullets">
<li>Journal Antiseptic Dec.1981 Pattern of ocular injuries</li>
<li>Indian Journal of Ophthalmology March 1985 33.89-90 role of Aspirin in cataract surgery</li>
<li>Afro - Asian Journal of Ophthalmology Vol XI No 2 September 1992</li>
<li>Afro - Asian Journal of Ophthalmology Vol XI No.3 December 1992</li>
<li>Contributed a chapter on "Ocular Tuberculosis" in the Text book of Tuberculosis edited by Dr.Satyasree.</li>
</ul>					</p>	
				</li>



				<li>

<h4><img src="images/dr_satyanarayana.jpg" alt="Dr.P.Satyanarayana">Dr.P.Satyanarayana, MBBS, 

M.S.</h4>
<h3>Comprehensive Ophthalmology</h3>
<p>
<ul class="bullets">
<li>He completed his graduation from Andhra Medical College - Visakhapatnam - 1976</li>
<li>Post Graduation in Ophthalmology from Rangaraya Medical College, Kakinada - Andhra University - 1982</li>
</ul>
</p>
<p>Dr.P.Satyanarayana is an experienced doctor worked as Assistant 

Professor for two tenures from 1988 - 1994 &amp; Nov 2006 - Oct 2008 and also as a Mobile Medical Officer 

in department of Ophthalmology - Rangaraya Medical College - Kakinada from 1995 to 

2008.</p><p>Post retirement from the Government Health Department he worked as Associate 

Professor of Ophthalmology from Nov 2008 - October 2010 and Professor of Ophthalmology from Nov 

2010 to April 2012 in Konaseema Institute of Medical Sciences - Amalapuram of East Godavari District.  

Apart from the above he served as Internal and External examiner to U.G and P.G students to N.T.R 

University (A.P) and M.G.R University - Tamilnadu</p><p>His publication has been published in Indian 

Journal of Chest Diseases and Allied Sciences - New Delhi, a cases report of Tubercular Nodular 

Episclerits.</p>
				</li>



<li>

<h4><img src="images/dr_padmini.jpg" alt="Dr.M.Padmini">Dr.M.Padmini, MBBS, M.S. (ophthalmology)</h4>
<ul class="bullets">
<li>Completed MBBS in 1st Division from Andhra Medical College, Visakhapatnam, 2005.</li>
<li>Passed M.S. in ophthalmology in 1st Division from Guntur Medical College, Guntur, 2011.</li>
<li>One year comprehensive fellowship in Ophthalmology at Srikiran Institute of ophthalmology.</li>
<li>Appointed as Junior consultant in ophthalmology at Srikiran Institute in 2011.</li>
</ul>
</li>



<li>

<h4><img src="images/dr_satish.jpg" alt="Dr.A.V.Satish">Dr.A.V.Satish, MBBS, M.S. (ophthalmology)</h4>
<ul class="bullets">
<li>Completed MBBS from Rangaraya Medical College, Kakinada in 2006.</li>
<li>Completed M.S. in ophthalmology from Rangaraya Medical College, Kakinada in 2012.</li>
<li>Appointed as Junior Consultant at Srikiran Institute of ophthalmology, Kakinada.</li>
</ul>
</li>

<li>

<h4><img src="images/dr_pushparajyam.jpg" alt="Dr.M.Pushpa Rajyam">Dr.M.Pushpa Rajyam, MBBS, D.O. (ophthalmology)</h4>
<ul class="bullets">
<li>Completed MBBS from Guntur medical College, Guntur, 1997.</li>
<li>Completed D.O. in ophthalmology from Rangaraya Medical College, Kakinada, 2005.</li>
<li>Joined for Fellowship in comprehensive ophthalmology at Srikiran Institute of Ophthalmology, Kakinada.</li>
</ul>
</li>
				
			</ul>			
		
		</div>		
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>