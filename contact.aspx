<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
contact.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Srikiran Institute of  Ophthalmology - Contact</title>	
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
	<meta name="keywords" content=""></meta>
	<meta name="description" content=""></meta>
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="icon" href="favicon.gif" type="image/gif" />
	<link rel="stylesheet" href="css/screen.css" media="screen" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
<!--[if IE 6]>
<script src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('.png, img');</script>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<![endif]-->       
    <style>
		ul.list li p{font:normal 11px verdana; line-height:18px}
	</style>
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro6" style="position:relative">
    		<p style="margin:79px 0 0 20px; font-size:16px; position:absolute"><span style="color:#FFCC66">Let's make a difference in others lives. Join our team in the eradication of blindness.</span></p>
    </div>     
   	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="contact.aspx">Contact</a></p>
	<div id="content">

		<div id="main" style="width:100%">
			<h2>Contact</h2>
            <p>Srikiran can be contacted at any one of these places.</p>
		<p><img src="images/flag_india.gif" border="0" width="24" height="24" align="absmiddle" /> <b>INDIA : Sankurathri Foundation</b><br />
			A.P.S.P.Post, <br />
            Kakinada-533005<br />
            Andhra Pradesh, INDIA<br />
			Ph: 0884-2306301, Fax: 0884-2306345<br />
			E-Mail: <a href="mailto:info@srikiran.org">info@srikiran.org</a>
		</p>
		<p><img src="images/flag_usa.gif" border="0" width="24" height="24" align="absmiddle" /><b> U.S.A: Kruti Dhata Inc</b><br />
        Please make your checks payable to <b>"Kruti Dhata"</b> (Tax Exempt organization under Section 501 (C)(3) of the Internal Revenue Code).
        <br />Federal Tax ID # 20-2411347<br />
		KRUTI DHATA Inc<br />
		Houston, TX 77258-8186<br />
        Website: <a href="http://www.kruti-dhata.org" target="_blank">www.kruti-dhata.org</a>
		</p>
 		<p><img src="images/flag_canada.gif" border="0" width="24" height="24" align="absmiddle" /><b> CANADA: Manjari Sankurathri Memorial Foundation</b><br />
			23 Mary Drive, Gloucester, Ontario, Pin: K1V 1G9<br />
			Tel.Fax: (613)523-5413<br />
			E-Mail: <a href="mailto:info@msmf.ca">info@msmf.ca</a><br />
            Website: <a href="http://www.msmf.ca" target="_blank">www.msmf.ca</a>
 		</p>

		</div>
	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>