﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
aboutus.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - About Us</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
	$('#navlist').show();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div class="pngns" id="intro2" style="position:relative">
    		<p style="margin:70px 0 0 20px; font-size:13px; position:absolute"><span style="color:#FFCC66">At Srikiran we are proud to announce that- <br />"No person is denied treatment for not being able to pay".</span></p>
    </div>    
	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="aboutUs.aspx">About us</a></p>        
	<div id="content">

		<div id="main">
            <h2><a name="whoweare"></a>Who We Are</h2>
			<p>With the visionary efforts of Dr. VK Raju, MD, and the Sankurathri Foundation, 'Srikiran' was established in 1993, offering unprecedented eye care in the region that was accessible to everyone. During its humble beginnings on January 21st, Srikiran had only 26 beds and a total staff of only 9. With the dedicated teamwork of the staff and firm commitment of the founders, the Institute has been able to grow and become unparalleled in its achievements.</p>
			<h2><a name="whyweare"></a>Why we are, who we are</h2>
            <p>There are over six billion people in the world. Over one billion (100 lakh) of these people reside in India. Around, 75%, or over 750,000,000 million people live in villages, and about 60% of these people are destitute, living well below the poverty line. Located on the southeastern coast, we find the state of Andhra Pradesh. Boasting an enormous 75 million (7.5 crores) inhabitants, Andhra Pradesh has enormous potential, but it is hindered by the fact that primary healthcare here is almost non-existent. It is believed that genetics, vitamin deficiency, and UV exposure combine to account for over 15 million (1.5 crores) Indians living without vision. Addressing these issues is a considerable task in itself, involving much time and effort. Through the coordinated efforts of a select few individuals and caring organizations, coupled with the desire to make a difference, The Srikiran Institute of Ophthalmology has taken some critical steps to resolve some of the fundamental issues that afflict the people in rural Andhra Pradesh. Located 10kms from Kakinada, the Srikiran Institute of Ophthalmology can be found on a five acre site that typifies the serene countryside in Andhra Pradesh. For over 15 years, the Institute has been addressing the needs of the local community, and has taken it upon itself to provide quality eye care that is both affordable and accessible to everyone.</p>
			<h2><a name="howweoperate"></a>How We Operate</h2>            
            <p>The modern facilities staffed by compassionate doctors continue to develop as the institute keeps on procuring the latest equipment necessary to provide quality eye care. Facing the prospect of providing eye care services to a population of over 20 million people from six different districts, Srikiran has developed into an all encompassing Institute spread out over an area of 50,000 square feet, with a total of five buildings.</p>
			<p>With separate outpatient areas for paying and non paying patients, ten outpatient clinics, four operating theatres, an auditorium, a cafeteria, an optical shop, a pharmacy, and two separate hostels for men and women, all backed-up by a generator room, Srikiran has become a self contained world providing the best care available. In addition to this, The Srikiran Institute now has 136 inpatient beds, and 24 hour emergency service, so proper care and instruction can be provided and any post-operative complications are reduced to the bare minimum and taken care of.</p>			
			<p>Reflecting its dynamic nature, the Institute is planning to develop into a centre of excellence in eye care for the region. This will include all specialties in ophthalmology and an eye bank. At the present time, Srikiran has eight ophthalmologists and one in training. In addition to this, there are 79 other full time employees indispensable to the Institute. These include 6 computer specialists, 13 administrative staff, 14 vision technicians, 3 outpatient personnel, 15 people within the operation theaters, 10 ward sisters, 8 transportation staff and 10 support services personnel, who make the Institute run so efficiently. Currently, there are also 36 trainees in various departments preparing themselves to assist in the appropriate sections of the eye hospital when needed.</p>
			<p>In order to make the Institute self-supporting, charges are levied only on patients who can afford to pay for the services. The services are all reasonably priced to make it affordable to all income groups. Whether paid patients or not, the high standards of excellence are upheld by providing the same medical service to all, regardless of socio-economic status. Services that are allotted to the paid category are based on medical treatment, procedures and the requirements of the patients. At Srikiran we are proud to announce that <span style="font-weight:bold; color:#FF3300">"No person is denied treatment for not being able to pay"</span>.</p>
			<p>Working efficiently, with a fluidity and organization that seems almost effortless the qualified people at Srikiran provide every service imaginable for both patients and staff. The modern facilities staffed by highly qualified doctors have provided unparalleled eye care to over 1, 3 23,215 out-patients and performed over 137,916 surgeries in Srikiran’s relatively short history.</p>
			<p>Gaining the confidence of the regional population, Srikiran now stands as a pioneer in providing quality eye-care services. To this date the Institute now has the capacity to treat 350 outpatients and 80 inpatients per day. The productive utilization of the available space, manpower, and the commitment of the team to provide excellent eye care have become major strengths on the path to eradicate preventable and curable blindness in the region. Coupled with this, the institute has been very fortunate to have continuous and dedicated support from the community in achieving these objectives. </p>
			<p>In recognition of this, the Government of Andhra Pradesh has successively awarded the Srikiran Institute the award for best Non Governmental Organization for its monumental work in the field of Ophthalmology and the sheer number of cataract surgeries performed in both 2002 and 2003. </p>


		<table id="mytable" cellspacing="0">
        <caption><strong></strong></caption>
          <tr>
            <th class="firstcell">Sankurathri Foundation Accomplishments (Jan, 2013)</th>        
            <th width="100">Cumulative upto Jan, 2013</th>
          </tr>
          <tr>
            <th class="specalt"><strong>Free Children Screening Camps</strong></th>
            <td class="alt">967</td>
          </tr>
          <tr>
            <th class="spec"><strong>School Children Screened</strong></th>
            <td>3,10,977</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Free Out-reach Camps in Rural Areas</strong></th>
            <td class="alt">1,861</td>
          </tr>
          <tr>
            <th class="spec"><strong>Outpatients Treated</strong></th>
            <td>18,60,938</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Surgeries Performed</strong></th>
            <td class="alt">1,82,587</td>
          </tr>
          <tr>
            <th class="spec"><strong>Workshops conducted</strong></th>
            <td>55</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Ophthalmologists Trained</strong></th>
            <td class="alt">56</td>
          </tr>
          <tr>
            <th class="spec"><strong>Para-medical Personnel Trained</strong></th>
            <td>344</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Support Staff Trained</strong></th>
            <td class="alt">109</td>
          </tr>
          <tr>
            <th class="spec"><strong>Vision Centers</strong></th>
            <td>4</td>
          </tr>
          <tr>
            <th class="spec"><strong>City Center</strong></th>
            <td>1</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Volunteers Visited</strong></th>
            <td class="alt">88</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Provided free education to children</strong></th>
            <td class="alt">1930</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Provided scholarships to children</strong></th>
            <td class="alt">336</td>
          </tr>
          <tr>
            <th class="specalt"><strong>Free Vocational Training courses to rural youth</strong></th>
            <td class="alt">216</td>
          </tr>
        </table>        

		</div>
		
		<!-- #INCLUDE file="right_nav.aspx" -->		        
	
	</div>	
</div>
		<!-- #INCLUDE file="footer.aspx" -->		 	
</body>
</html>