﻿<div id="side">
  <div class="pngns top_bg"></div>
  <div class="png mid_bg">
    <ul id="navlist" style="display:none">
      <li class="pngns" id="<asp:Literal id='background' runat='server' />"><asp:HyperLink NavigateUrl="background.aspx" runat="server" Text="Background of Srikiran Institute of Ophthalmology" /></li>
      <li class="pngns" id="<asp:Literal id='achievements' runat='server' />"><asp:HyperLink NavigateUrl="achievements.aspx" runat="server" Text="Srikiran's Achievements up to December 2008" /></li>
    </ul>
    <p id="galHdr"> <img src="images/grass.png" border="0"  align="absmiddle"/>&nbsp;Gallery</p>
    <div id="gallery">
      <div class="imgHldr"> <a href="photos/photo1.jpg" title="Staff at Srikiran Institute of Ophthalmology"><img src="images/img1.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo2.jpg" title="Rural eye camp"><img src="images/img2.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo3.jpg" title="Patients returning home after receiving treatment at Srikiran Institute"><img src="images/img3.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo4.jpg" title="Patient undergoing examination at rural eye camp"><img src="images/img4.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo5.jpg" title="Patient undergoing examination at rural eye camp"><img src="images/img5.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo6.jpg" title="Patient undergoing examination at rural eye camp"><img src="images/img6.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo7.jpg" title="In the operating room at Srikiran Institute of Opthalmology"><img src="images/img7.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo8.jpg" title="Patient being examined at Srikiran Institute"><img src="images/img8.png" border="0" /></a> </div>
      <div class="imgHldr"> <a href="photos/photo9.jpg" title="Humphrey's visual field analyzer for Glaucoma"><img src="images/img9.png" border="0" /></a> </div>      
      <div class="imgHldr"> <a href="photos/photo10.jpg" title="Counseling for patients who had undergone surgery"><img src="images/img10.png" border="0" /></a> </div>            
      <div class="imgHldr"> <a href="photos/photo11.jpg" title="Counseling for patients who had undergone surgery"><img src="images/img11.png" border="0" /></a> </div>                  
      <div class="imgHldr"> <a href="photos/photo12.jpg" title="Workshop in progress at Srikiran"><img src="images/img12.png" border="0" /></a> </div>                  
      <div class="imgHldr"> <a href="photos/photo13.jpg" title="Video cast of the operating room proceedings being watched remotely"><img src="images/img13.png" border="0" /></a> </div>
    </div>
  </div>
  <div class="pngns btm_bg"></div>
</div>
