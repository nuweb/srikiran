<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
services.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Services</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro3" style="position:relative">
    		<p style="margin:70px 0 0 20px; font-size:16px; position:absolute"><span style="color:#FFCC66">Srikiran has been awarded as the best non-governmental organization <br />for its monumental work in the field of Ophthalmology.</span></p>
    </div>    

   	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="services.aspx">Services</a></p>        
	<div id="content">

		<div id="main">
			<h2>Services at Srikiran</h2>
			<h3>Outpatients</h3>
			<p>Outpatient services are open every day of the week from 8am to 6pm, except Sunday. The comprehensive eye care services have grown significantly since 1993, but ambitions do not stop there. Specialty clinics are also started focusing on the cornea, glaucoma, Vitreo&ndash;retinal problems, pediatric services, low vision rehabilitation, microbiology laboratory and optical industry. The quality of care offered to both the paying patients and non&ndash;paying patients upholds the same standards of excellence that has been with the institute since its inception.</p>
			<h3>Surgeries</h3>
			<p>Since 1993, the Institute has steadily progressed in both the quantity and quality of surgeries performed. Keeping up to date with modern concepts and procedures, Srikiran offers a level of care previously unseen in the region as it operates everyday of the week except Sunday. Although the Institute performs a wide variety of surgeries, they have targeted the local needs and place a needed focus on the cataract surgery. Intraocular lens implantation has been offered to all patients since 1993, and since 1997 the SICS or Small Incision Cataract Surgery has become the predominant outpatient procedure. Another method of surgery known as Phacoemulsification or 'PHACO” is offered by the Institute which increases the patients' range of choice, as well as expands the training of the staff. </p>
			<h3>Transportation</h3>
			<p>When eye care is needed, but it is not easily obtained, Srikiran Institute have adopted the ingenious solution of bringing the eye care right to the doorsteps of those in need. Overcoming the issue of accessibility, free transportation is provided to all people attending the Institute. Operating from 7:45am to 6:30pm, three buses make frequent trips to multiple locations in the area including Bhanugudi Junction, Kakinada. With this unique approach, Srikiran has also developed community outreach programs based in the remote and rural areas of East Godavari, West Godavari, Visakhapatnam, and Khammam districts. These are discussed in more detail below. </p>
			<h2>Medical Programs Offered by Srikiran</h2>
			<h3>Glaucoma Unit</h3> 
            <p>The Institute's commitment to serving the needs of the local community by focusing on the preventable causes of blindness has prompted the design of a Glaucoma Screening Program. Since its inception, the program has evolved into a standard procedure for all the patients attending the institute. We have all the necessary equipment and skilled personnel to diagnose and treat the patients to prevent blindness.</p>
			<h3>Cornea Unit</h3>
            <p>This unit is available on all working days. The most common complaints are the corneal injuries, ulcers because of the agricultural work force in the area. In addition, there are several cases of pterygia which are recurring type and they are being treated with amniotic membrane grafting. Corneal transplantations are also conducted regularly when the donor tissues are available.</p>
			<h3>Retina Unit</h3>
            <p>Srikiran has a full fledged retina unit with B&ndash;Scan, fundus camera, two green lasers, red laser, and instruments for all retinal surgeries. Medical retinal services are provided on all working days and surgeries are conducted on selected days.</p>
			<h3>Pediatric Unit</h3>
            <p>A special pediatric unit is designed with the support and cooperation of ORBIS International. Srikiran also has a team of ten specially trained personnel in dealing with children. All diagnostic rests and surgeries are performed regularly for squint, congenital cataracts and Glaucomas, trauma cases, and other pediatric cases.</p>
			<h3>Low vision assessment and Rehabilitation unit</h3>
            <p>Low vision and rehabilitation unit is available with four specially trained team of personnel to exclusively attend to those cases. Several devises are also made available for the people to purchase at the Institute.</p>
		</div>		
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->		 	
</body>
</html>