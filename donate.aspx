<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
donate.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Donate</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro5" style="position:relative">
    		<p style="margin:79px 0 0 20px; font-size:16px; position:absolute"><span style="color:#FFCC66">Let's light a lamp in the life of the blind.</span></p>
    </div>      
   	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="donate.aspx">Donate</a></p>    
	<div id="content">

		<div id="main">
			<h2>Donate</h2>
            <h3>How you can help</h3>
			<p>Since its inception, the Srikiran Institute of Ophthalmology has provided eye care facilities to over 1,370,607 patients in need, and performed over 142, 358 cataract surgeries without charge.   All this was and is only possible with the involvement of many kind philanthropists and institutions. Even after all these efforts, there are still many people who need help to improve their quality of life. In India, there is a backlog of 12 million cataracts to be operated on, and the problem is even more prominent in rural areas. Your help will be useful in maintaining the quality of service, in making services affordable as well as available, strengthening the infrastructure, and continuing to provide free service to those who are really in need.</p>
			<p>You can become a part of us by:</p>
			<ul>
            	<li>Volunteering your time and skills at the institute (depending on qualifications and experience)</li>
				<li>Identifying persons with eye problems and sending them to the Institute.</li>
				<li>Donating much needed building materials, furniture, equipment or any other materials. </li>
				<li>By donating money to the corpus fund. </li>
				<li>Donating for cataract surgeries and sponsoring of screening clinics in a village. </li>
				<li>Contributions are exempted from tax in India, US and Canada.</li>
				<li>The Foundation has permission to receive donations from abroad under the Foreign Contribution Regulation Act.</li>
			</ul>
            <p>Please come forward, we need you.</p>
			<p>Managing Director, <br />Sankurathri Foundation,<br />A.P.S.P. Camp Post,<br />Kakinada- 533 005<br />
            Andhra Pradesh<br /><br />Tel: (0884) 230 6301<br />Fax: (0884) 230 6345</p>
			<p class="light">Let us light a lamp in the life of the blind</p>
		</div>
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>