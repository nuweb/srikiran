﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
home.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Home</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<!--[if IE 6]>
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<![endif]-->
<style>
div#homebox{padding:15px 0 0 35px; font-size:20px; color:#fff}
div#homebox ul li,div#homebox ul li a{font:normal 11px verdana; color:#fff; line-height:22px; text-decoration:none}
div#homebox ul li a:hover{text-decoration:underline}
div#homebox ul {padding-left:16px; margin:-10px 0 0 0}
a.extSite{
   padding-right: 18px;
   background: transparent url(images/icon_external_site.gif) no-repeat center right;
}
a.extSiteWht{
   padding-right: 18px;
   background: transparent url(images/icon_external_site_wht.gif) no-repeat center right;
}
</style>
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->
	<div id="intro" class="pngns"></div>

	<div class="col first">
	
		<h2>Why we are, who we are</h2>	
		<p><img src="images/whatwedo.png" alt="" /></p>	
		<p>It is believed that genetics, vitamin deficiency, and UV exposure combine to account for over 15 million (1.5 crores) Indians living without vision. Addressing these issues is a considerable task in itself, involving much time and effort...<br /><a href="aboutUs.aspx#whyweare" title="Learn more about why we are, who we are"><span>Learn more.</span></a></p> 
  </div>
	
	<div class="col">
	
		<h2>How we operate</h2>	
		<p><img src="images/howwedo.png" alt="" /></p>	
		<p>The modern facilities staffed by compassionate doctors continue to develop as the institute keeps on procuring the latest equipment necessary to provide quality eye care.  Facing the prospect of providing eye care services to a population of over 20 million...<a href="aboutUs.aspx#howweoperate" title="Learn more about how we operate"><span>Learn more.</span></a></p> 
	</div>
	
	<div class="col">
	
		<h2>History</h2>	
		<p><img src="images/history.png" alt="" /></p>	
		<p><a class="extSite" href="http://msmf.ca/" target="_blank">Sankurathri Foundation</a> was established by <b>Dr.Chandrasekhar Sankurathri</b> as a memorial to his wife, Manjari Sankurathri, son, Srikiran and daughter, Sarada who were killed in a terrorist bombing of Air India Flight 182 (Kanishka) on 23rd June 1985.<strong>..</strong><a href="background.aspx" title="Learn more about the history of Srikiran"><span>Learn more.</span></a></p> 
	</div>		

<br clear="all" />
	<div id="content">

		<div id="main">
		
			<h2>Community Outreach Program</h2>
			<p><div style="float:right; text-align:center; font:normal 11px verdana"><img style="margin:10px" src="images/adultCamp.png" width="200" height="150" /><div>Eye screening camp at a village.</div></div></p>
		    <p>Some highlights of the program, <span style="font-weight:bold; color:#FF3300">all provided free of charge</span>, are as follows:</p>
			<ul>
            <li>Providing eye care to the poor.</li>
			<li>Organizing eye screening clinics three days in a week.</li>
			<li>Providing transportation to the base hospital if surgery is required.</li>

			<li>Providing intraocular lens implant.</li>
			<li>Providing food, accommodation and medicines.</li>
			</ul>
			
			<p class="link"><a href="programs.aspx" title="Learn more about community outreach program"><span>Learn more.</span></a></p>
					
		</div>	
        
		<div class="" id="side" style="margin-top:10px;height:255px;background-image:url(images/home_cnn.png)">	
        <div id="homebox">
        <p>CNN Heroes 2008</p>
        <ul>
        	<li><a class="extSiteWht" href="http://www.cnn.com/2008/LIVING/wayoflife/08/15/heroes.sankurathri/index.html" target="_blank">His tragic loss helps others gain sight</a></li>
			<li><a class="extSiteWht" href="http://www.cnn.com/video/#/video/living/2008/08/13/heroes.sankurathri.profile.cnn" target="_blank">Help for the blind</a></li>
            <li><a class="extSiteWht" href="http://www.cnn.com/video/#/video/living/2008/08/13/heroes.sankurathri.extra1.cnn" target="_blank">"Saving sight in India"</a></li>
            <li><a class="extSiteWht" href="http://www.cnn.com/SPECIALS/cnn.heroes/archive/chandrasekhar.sankurathri.html" target="_blank">2008 CNN Heroes Archive</a></li>
        </ul>    
        <p>CBC News Online 2005</p>	
        <ul>
        	<li><a class="extSiteWht" href="http://www.cbc.ca/news/background/airindia/milewski_rayoflight.html" target="_blank">A ray of light.</a></li>
        </ul>    	
        </div>
		</div>	        
	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>
