<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
volunteer.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Volunteer</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro7" style="position:relative">
    		<p style="margin:79px 0 0 20px; font-size:16px; position:absolute"><span style="color:#FFCC66">We need your participation, please join our volunteer program.</span></p>
    </div>      
   	<p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="volunteer.aspx">Volunteer</a></p>
	<div id="content">
		<div id="main">
			<h2>Volunteer</h2>
			<p>Srikiran's continuous innovation and strength is due to the support of several extraordinary volunteers since 1993. These people have made immense contributions towards advancing the standards of eye care and the quality of overall services. We gratefully acknowledge the assistance of the following volunteers who have provided their unwavering support over the years.</p>
            <div class="listWrapper">
            <ul
                ><li>Dr. David Addison</li
                ><li>Dr. David Knox</li
                ><li>Dr. Karim Damji</li
                ><li>Dr. Peter Kertes</li
                ><li>Dr. V.K Raju</li
                ><li>DR. Linda Lawrence</li
                ><li>Dr. Gene Helveston</li
                ><li>Dr. Brian Lorimar</li
                ><li>Dr. Vinod Mootha</li
                ><li>Dr. Kjell Dahlen</li
                ><li>Dr. Sanjoy Gupta</li
                ><li>Dr. Aravind Chandna</li
                ><li>Ms. Rosemary Bickerton</li
                ><li>Dr. Rob Campbell</li
                ><li>Dr. Otis Paul</li
                ><li>Dr. Matthew Farber</li
                ><li>Dr. Anette D’Souza</li
                ><li>Dr. Mike Hatfield</li
                ><li>Dr. Michael Clarke</li
                ><li>Dr. Dave Marshall</li
                ><li>Dr.Gault Farrell</li
            ></ul>
            <br />
            </div>
            
			<h3>Our Thanks</h3>
			<p>Many well wishers and supporters have contributed with a benevolent heart to the progress and sustenance of the Institute. The accomplishments to this date are due to the commitment and dedication of many friends, volunteers, doctors, paramedical and support staff.</p>
			<p>The confidence and trust exhibited by the patients are a strong testament to the competence and skills of the staff at Srikiran in their work providing world-class eye care.</p>
			<p>Over the years, countless voluntary organizations, corporations, and philanthropists have extended their help in sponsoring free eye clinics in the Community Outreach Program. By supporting the Institute in fight against the elimination of avoidable and curable blindness, the following individuals and organizations have shared their inspiration and wisdom. Without them, the successive growth and sustainability we have seen within the Institute would not have been possible. </p>
            <ul> 
                <li>Aravind Eye Care Systems, Madurai.</li>
                <li>Canadian International Development Agency (CIDA), Canada.</li>
                <li>Christoffel Blinden Mission International (CBM), Benshiem, Germany.</li>
                <li>District Blindness Control Society (DBCS)</li>
                <li>Eye Foundation of America, Morgantown, West Virginia, USA.</li>
                <li>Help Age India</li>
                <li>Help the Aged Canada, Ottawa, Canada.</li>
                <li>Manjari Sankurathri Memorial Foundation (MSMF), Ottawa, Canada.</li>
                <li>National Program for Control of Blindness, (NPCB)</li>
                <li>ORBIS International, New York, U.S.A</li>
                <li>Rotary International</li>
                <li>Rotary Club of Kakinada</li>
                <li>Rotary Club of Plattsburgh, New York, USA.</li>
                <li>Dr. Kjell Dahlen, Eye Care for the Adirondacks, Plattsburgh, NY, USA.</li>
                <li>Mr. Satish Chandra, IAS, Collector, EG Dist.</li>
			</ul>
			<h2>Visitor Impressions</h2>		
						
			<ul class="list">
				<li class="first">
					<p>This is truly an oasis in more ways than one. I was moved beyond tears. Thank you for your kindness, generosity and inspiration."<br /><span class="light">&ndash;Cornelia Princepe, Reporter, Toronto, Canada.</span></p>
				</li>
				<li>
					<p>"An inspiration of supreme sacrifice out of tragedy. This is the result of God’s Plan. Thanks for your generous hospitality."<br /><span class="light">&ndash;Dr. and Mrs. E.B Sundaram, NIRPHAD, Delhi.</span></p>
				</li>							
				<li>
					<p>"Thank you for allowing me to visit and see the excellent care that you provide. I am impressed by the level of dedication shown by everyone involved."<br /><span class="light">&ndash;Dr. Kjell Dahlen, Eye Care for the Adirondacks, Plattsburgh, USA.</span></p>
				</li>							
				<li>
					<p>"It was a delight to meet all of you and to see first hand the wonderful work that you are doing. We hope this leads to greater Canadian involvement. Thank you."<br /><span class="light">&ndash;Murray and Eilis Hiebert, Calgary, Alberta, Canada.</span></p>
				</li>							
				<li>
					<p>"Thank you an oasis. I hope the work for blind children has been conceived today."<br /><span class="light">&ndash;Dr. Allan Foster, Christoffel Blinden Mission, Germany.</span></p>
				</li>							
				<li>
					<p>"We are impressed by your good work and we hope to hear more from you!"<br /><span class="light">&ndash;Claudia Koenig, Christoffel Blinden Mission, Regional Office, Bangalore.</span></p>
				</li>
			</ul>            
		</div>
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>