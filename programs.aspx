<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<script runat="server">
void Page_Load(Object Sender, EventArgs e) {
string thispage = "selected";
programs.Text = thispage;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Srikiran Institute of  Ophthalmology - Programs</title>	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
<meta name="keywords" content=""></meta>
<meta name="description" content=""></meta>
<meta http-equiv="imagetoolbar" content="no" />
<link rel="icon" href="favicon.gif" type="image/gif" />
<link rel="stylesheet" href="css/screen.css" media="screen" />
<link rel="stylesheet" href="css/tables.css" media="screen" />    
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="css/ie6.css" media="screen" />
<link rel="stylesheet" href="css/png_fix.css" media="screen" />
<style>#side{margin-left:40px}</style>
<![endif]-->       
<script type="text/javascript">
$(function() {
	$('#gallery a').lightBox();
});
</script>   
</head>
<body>

<div id="container">
	<!-- #INCLUDE file="header.aspx" -->		 	
	<div id="intro4" style="position:relative">
    		<p style="margin:70px 0 0 20px; font-size:16px; position:absolute"><span style="color:#FFCC66">Srikiran takes eye care to the doorsteps of the needy in remote and rural areas through <br />the community outreach programs.</span></p>
    </div>    
    <p id="crumb"><a href="index.aspx">Home</a> <span class="arrow1">&nbsp;</span> <a href="programs.aspx">Programs</a></p>
	<div id="content">

		<div id="main">
			<h2>Programs at Srikiran</h2>
			<h3>Community Outreach Program</h3>
			<p>Instead of taking a passive stance and waiting for patients that may not come, Srikiran has adopted the unique approach of bringing its services to the doorsteps of people in need. By going to rural and remote areas, this unique program works closely with local community leaders and service groups to organize free eye screening clinics.  Several hundred patients can be screened in a single day, whereas before they might not have even received care. </p>
			<p>Some highlights of the program, <span style="font-weight:bold; color:#FF3300">all provided free of charge</span>, are as follows:</p>
			<ul>
            <li>Providing eye care to the poor.</li>
			<li>Organizing eye screening clinics three days in a week.</li>
			<li>Providing transportation to the base hospital if surgery is required.</li>
			<li>Providing intraocular lens implant.</li>
			<li>Providing food, accommodation and medicines.</li>
			<li>Providing transportation for three post&ndash;operative follow&ndash;up visits.</li>
			<li>Providing corrective glasses if required after six weeks of surgery. </li>
			</ul>

			<p>What makes the program unique in its approach is the blend of empathy, compassion and love that has opened it up to the community. Acceptance by the local population has increased greatly over the years and has led to an unprecedented level of trust. Under this program, approximately 1,290 eye screening clinics have been conducted. As a result, many people from remote areas are receiving treatment that would otherwise go untreated, and in turn, the Institute is increasing the quality of life for countless people. </p>

			<h4>'Cataract Free' Zone</h4>
			<p>The concept of this program, as it suggests, is to select a small geographical area and eradicate all presence of cataracts. With a population of 16,000, the Chebrolu village has shown great interest in the concept. After extensive work and countless operations, Dr. G. Sitaramaswamy, along with the cooperation of the locals are working to make this goal a reality. This village will become a trendsetter and a model for others to adopt and strive towards. Following this example, the fight to eradicate the backlog cases of cataracts will be that much closer to completion. </p>

			<h4>Children's Out&ndash;reach Program</h4>
			<p>After conducting an initial survey around 1,400 children from various schools in the area, a detailed study was undertaken to begin screening children from all the schools in the vicinity. The study is in progress and will continue further in order to prevent childhood blindness in the region. Any children in need will be treated free of charge for disorders such as vitamin A deficiency, refractive errors, squint eye, cataracts, and any other problems. </p>

			<h3>Teaching, Training and Educational Programs</h3>
			<p>As a recognized training center by the Government of India, Srikiran has gained distinction by becoming one of a mere eleven training centers that qualify the staff to train IOL surgeries. By placing such a great emphasis on education, all the staff members, ophthalmologists, and general physicians are kept up to date with all the latest developments in ophthalmology and eye care. To obtain the high standards needed by the institute, various programs are in place to train the staff and keep everyone well informed. These are discussed below.</p>
            <h4>Fellowships</h4>
			<p>Offered to recent postgraduates in Ophthalmology, the fellowship program offers to teach the candidate all the latest techniques, concepts, and treatments in eye care. The duration of the program is one year, allowing enough time for the candidate to learn to apply the skills learned.    In addition, several ophthalmologists from Government of India were trained in cataract surgery and IOL implantation techniques. To this date, 72 ophthalmologists were successfully completed this training program.</p>
			<h4>Workshops</h4>
			<p>Workshops aimed towards teaching previously qualified ophthalmologists the most recent developments in the field are conducted mostly at the time of visiting consultants. Over 63 workshops have been organized and 1,575 ophthalmologists have attended these workshops.</p>
			<h4>Observerships</h4>
			<p>Over the years, the Institute has received a high volume of interest about the way in which it manages the high volume of cataract surgeries while maintaining the quality of service. Due to the expressed interest of doctors in observing manual Small Incision Cataract Surgery, an observation program has been setup for their benefit. Under this program, usually two outside ophthalmologists attend the institute two days a week to observe the general procedures. This program has helped them to organize their work better and also to improve the quality of their surgeries.</p>			
            <h4>Continuing Medical Education Program</h4>
			<p>The goal of creating a network of exchange between different specialties of medicine has led to the creation of this program. Offered to general physicians who are interested in learning the latest developments in ophthalmology, this program helps doctors outside the Institute guide their patients to seek timely help from ophthalmologists. So far, five such C.M.E courses have been conducted with over 160 doctors attending.</p>			
			<h4>Ophthalmic Assistants Training</h4>
 			<p>Due to the overwhelming need for more qualified and competent ophthalmic assistants, a one year training course was developed. The course is aimed at providing occupational skills to young persons with a high school completion background. The course is completely free, and a stipend is also provided to the candidates. To date, 215 young people have been offered this unique opportunity, and among them 80% were women from poor families.</p>
			<h4>General Awareness</h4>
            <p>To address the lack of understanding in the general public, educational seminars have been setup to educate people about the eye, the possible problems, eye care, and available treatments. In addition to spreading awareness, and taking strong preventative measures, the Srikiran Institute strives to dispel the many myths and misconceptions through this multi-faceted education program. The methods adopted so far include:</p>
			<ul>
            	<li>Proper counseling of the patients regarding their eye problems and the choices of treatment available</li>
                <li>Educating the teachers in the Children’s Outreach Program</li>
                <li>Advising the patients on one-on-one basis during the Community Outreach Program and also at the base hospital </li>
				<li>Preparing and distributing brochures related to eye care, in both English and Telugu</li>
                <li>Playing video discs about eye care at the Institute while patients are waiting</li>
            </ul>
		</div>	
		<!-- #INCLUDE file="right_nav.aspx" -->		        	
	</div>	
</div>
<!-- #INCLUDE file="footer.aspx" -->
</body>
</html>